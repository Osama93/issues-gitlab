import os
import time
from datetime import datetime
from urllib import parse
from urllib.parse import urlencode
import dateutil.parser
import requests
import schedule
from dotenv import load_dotenv
from influxdb import InfluxDBClient

load_dotenv()

group_id = str(os.getenv('GROUP_ID'))

client = InfluxDBClient(host=os.getenv('DATABASE_HOST'), port=os.getenv('DATABASE_PORT'),
                        username=os.getenv('DATABASE_USER'),
                        password=os.getenv('DATABASE_PASS'))

client.create_database("frontenddb")

client.switch_database("frontenddb")


def build_url(base_url, path, args_dict):
    url_parts = list(parse.urlparse(base_url))
    url_parts[2] = path
    url_parts[4] = urlencode(args_dict)
    return parse.urlunparse(url_parts)


def send_request():
    global current_time
    global current_date
    json_body = []

    # subgroups_json_body = []
    projects_json_body = []
    closed_json_body = []

    labels_list = ['Todo', 'TODO', 'ToDo', 'todo', 'toDo', 'to do', 'To do', 'to Do', 'TO DO', 'to_do', 'To_do',
                   'to_Do', 'TO_DO', 'doing', 'Doing', 'DOING', 'Review', 'review', 'REVIEW']
    page_num = 1
    projects_page_num = 1
    closed_page_num = 1

    repository_path = f'/api/v4/groups/{group_id}/issues'
    projects_repository_path = f'/api/v4/groups/{group_id}/projects'
    closed_repository_path = f'/api/v4/groups/{group_id}/issues'

    while projects_page_num:

        args = {'simple': 'true', 'include_subgroups': 'true', 'per_page': '100', 'page': f'{projects_page_num}'}
        base_url = f'https://gitlab.com/'
        url1 = build_url(base_url, projects_repository_path, args)
        print(url1)
        projects_json_body_ = requests.get(str(url1), headers={'PRIVATE-TOKEN': os.getenv('TOKEN')}).json()
        if not projects_json_body_:
            break
        projects_page_num = projects_page_num + 1
        projects_json_body.append(projects_json_body_)
    # print(projects_json_body)

    while page_num:

        args = {'scope': 'all', 'state': 'opened', 'assignee_id': 'Any', 'per_page': '100', 'page': f'{page_num}'}
        base_url = f'https://gitlab.com/'
        url1 = build_url(base_url, repository_path, args)
        print(url1)
        json_body_ = requests.get(str(url1), headers={'PRIVATE-TOKEN': os.getenv('TOKEN')}).json()
        if not json_body_:
            break
        page_num = page_num + 1
        json_body.append(json_body_)

    while closed_page_num:

        args = {'scope': 'all', 'state': 'closed', 'assignee_id': 'Any', 'per_page': '100', 'page': f'{closed_page_num}'}
        base_url = f'https://gitlab.com/'
        url1 = build_url(base_url, closed_repository_path, args)
        print(url1)
        closed_json_body_ = requests.get(str(url1), headers={'PRIVATE-TOKEN': os.getenv('TOKEN')}).json()
        if not closed_json_body_:
            break
        closed_page_num = closed_page_num + 1
        closed_json_body.append(closed_json_body_)



    projects_json_page_index = 0
    id_list = []
    name_list = []

    while projects_json_page_index < len(projects_json_body):
        for projects_json_body_index in range(len(projects_json_body[projects_json_page_index])):
            id_list.append(projects_json_body[projects_json_page_index][projects_json_body_index]['id'])
            name_list.append(projects_json_body[projects_json_page_index][projects_json_body_index]['name'])

        projects_json_page_index = projects_json_page_index + 1

    my_dict = dict(zip(id_list, name_list))
    # print(my_dict)


    closed_json_page_index = 0
    closed_reference_list = []

    while closed_json_page_index < len(closed_json_body):
        for closed_json_body_index in range(len(closed_json_body[closed_json_page_index])):
            closed_reference_list.append(closed_json_body[closed_json_page_index][closed_json_body_index]['references']['full'])

        closed_json_page_index = closed_json_page_index + 1






    json_page_index = 0
    my_json = []
    # todo_json = []
    # doing_json = []
    # review_json = []
    my_json_1 = []
    my_json_2 = []
    project_name = ''
    label_lowercase = ''

    while json_page_index < len(json_body):
        for json_body_index in range(len(json_body[json_page_index])):



            for key in my_dict:
                if key == json_body[json_page_index][json_body_index]['project_id']:
                    project_name = my_dict[key]

            if json_body[json_page_index][json_body_index]['labels'] == []:
                label_lowercase = 'todo'
            elif json_body[json_page_index][json_body_index]['labels'] != []:

                for label in labels_list:

                    if label in json_body[json_page_index][json_body_index]['labels']:

                        indexOfLabel = json_body[json_page_index][json_body_index]['labels'].index(label)

                        if "_" in label:
                            label_formatted = json_body[json_page_index][json_body_index]['labels'][
                                indexOfLabel].replace("_", "")
                        elif " " in label:
                            label_formatted = json_body[json_page_index][json_body_index]['labels'][
                                indexOfLabel].replace(" ", "")
                        else:
                            label_formatted = json_body[json_page_index][json_body_index]['labels'][indexOfLabel]

                        label_lowercase = label_formatted.lower()

                    elif label not in json_body[json_page_index][json_body_index]['labels']:
                        x = set(labels_list)
                        y = set(json_body[json_page_index][json_body_index]['labels'])
                        if x.isdisjoint(y):
                            label_lowercase = 'others'

            my_json.append({
                "measurement": "issues",
                "tags": {
                    "assignee_username": json_body[json_page_index][json_body_index]['assignee']['username'],
                    "project_name": project_name,
                    "labels": label_lowercase,
                    "issue_id_tag": json_body[json_page_index][json_body_index]['references']['full']
                },
                "time": json_body[json_page_index][json_body_index]['updated_at'],
                "fields": {
                    "state": json_body[json_page_index][json_body_index]['state'],
                    "labels": label_lowercase
                }
            })

            my_json_1.append({
                "measurement": "resource",
                "tags": {
                    "assignee_username": json_body[json_page_index][json_body_index]['assignee']['username'],
                    "project_name": project_name,
                    "labels": label_lowercase,
                    "issue_id_tag": json_body[json_page_index][json_body_index]['references']['full'],
                    "date": str(datetime.now().date())
                },
                "time": str(datetime.now().date()),
                "fields": {
                    "state": json_body[json_page_index][json_body_index]['state'],
                    "labels": label_lowercase
                }
            })


            #### [Incomplete} Time taken from todo to doing and from doing to review
            # if label_lowercase == 'todo':
            #     todo_json.append({
            #         "measurement": "todo",
            #         "tags": {
            #             "assignee_username": json_body[json_page_index][json_body_index]['assignee']['username'],
            #             "project_name": project_name,
            #             "labels": label_lowercase,
            #             "issue_id_tag": json_body[json_page_index][json_body_index]['references']['full']
            #         },
            #         "time": json_body[json_page_index][json_body_index]['updated_at'],
            #         "fields": {
            #             "state": json_body[json_page_index][json_body_index]['state'],
            #             "labels": label_lowercase
            #         }
            #     })
            #
            # elif label_lowercase == 'doing':
            #     doing_json.append({
            #         "measurement": "doing",
            #         "tags": {
            #             "assignee_username": json_body[json_page_index][json_body_index]['assignee']['username'],
            #             "project_name": project_name,
            #             "labels": label_lowercase,
            #             "issue_id_tag": json_body[json_page_index][json_body_index]['references']['full']
            #         },
            #         "time": json_body[json_page_index][json_body_index]['updated_at'],
            #         "fields": {
            #             "state": json_body[json_page_index][json_body_index]['state'],
            #             "labels": label_lowercase
            #         }
            #     })
            # elif label_lowercase == 'review':
            #     review_json.append({
            #         "measurement": "review",
            #         "tags": {
            #             "assignee_username": json_body[json_page_index][json_body_index]['assignee']['username'],
            #             "project_name": project_name,
            #             "labels": label_lowercase,
            #             "issue_id_tag": json_body[json_page_index][json_body_index]['references']['full']
            #         },
            #         "time": json_body[json_page_index][json_body_index]['updated_at'],
            #         "fields": {
            #             "state": json_body[json_page_index][json_body_index]['state'],
            #             "labels": label_lowercase
            #         }
            #     })

        json_page_index = json_page_index + 1

    get_json = client.query("SELECT * FROM issues")
    get_json_list = list(get_json.get_points(measurement="issues"))

    reference_list = []
    for user in get_json_list:
        reference_list.append(user['issue_id_tag'])


    for measurement1 in my_json:
        for measurement2 in get_json_list:

            if measurement1['tags']['issue_id_tag'] == measurement2['issue_id_tag'] and measurement1['tags']['labels'] != measurement2['labels']:
                x = measurement2['issue_id_tag']
                y = "\'" + x + "\'"
                print(y)
                client.query(f'DELETE FROM issues WHERE issue_id_tag = {y}')
    client.write_points(my_json)


    # print(closed_reference_list)
    # print(reference_list)


    for x in reference_list:
        if x in closed_reference_list:
            y1 = "\'" + x + "\'"
            client.query(f'DELETE FROM issues WHERE issue_id_tag = {y1}')

    temp1 = []
    temp2 = []
    status = ''


    get_resource_json = client.query("SELECT * FROM resource")
    get_resource_json_list = list(get_resource_json.get_points(measurement="resource"))

    reference_list_1 = []
    for user in get_resource_json_list:
        reference_list_1.append(user['issue_id_tag'])

    for measurement1 in my_json_1:
        for measurement2 in get_resource_json_list:
            if measurement1['tags']['issue_id_tag'] == measurement2['issue_id_tag'] and measurement1['tags']['date'] == measurement2['date'] and measurement1['tags']['labels'] != measurement2['labels']:
                x1 = measurement2['issue_id_tag']
                y1 = "\'" + x1 + "\'"
                x2 = measurement2['date']
                y2 = "\'" + x2 + "\'"
                # print(y)
                client.query(f'DELETE FROM resource WHERE issue_id_tag = {y1} AND date = {y2}')
    client.write_points(my_json_1)

    for x in reference_list_1:
        if x in closed_reference_list:
            y1 = "\'" + x + "\'"
            y2 = "\'" + str(datetime.now().date()) + "\'"
            client.query(f'DELETE FROM resource WHERE issue_id_tag = {y1} AND date = {y2}')

    for user in get_resource_json_list:
        temp1.append(user['assignee_username'])

    client.query("SELECT COUNT(labels) INTO occupation FROM resource WHERE labels='doing' GROUP BY assignee_username,date")

    for user in temp1:
        y1 = "\'" + user + "\'"
        y2 = "\'" + str(datetime.now().date()) + "\'"
        d = client.query(f"SELECT COUNT(labels) FROM resource WHERE labels='doing' AND assignee_username={y1} and date={y2}")
        d_list = list(d.get_points(measurement="resource"))
        if not d_list:
            client.query(f"DELETE FROM occupation WHERE assignee_username={y1} AND date={y2}")

    y = "\'" + str(datetime.now().date()) + "\'"

    get_occupation_json = client.query(f"SELECT * FROM occupation WHERE date={y}")
    get_occupation_json_list = list(get_occupation_json.get_points(measurement="occupation"))
    for user in get_occupation_json_list:
        temp2.append(user['assignee_username'])

    for user in temp1:
        if user not in temp2:
            status = 'free'
            my_json_2.append({
                    "measurement": "resource_occupation",
                    "tags": {
                        "assignee_username": user,
                        "date_tag": str(datetime.now().date()),
                        "status_tag": status
                    },
                    "time": str(datetime.now().date()),
                    "fields": {
                        "status": status,
                        "date": str(datetime.now().date())
                    }
                })

    for user1 in get_occupation_json_list:
        if user1['date'] != str(datetime.now().date()):
            continue
        elif user1['date'] == str(datetime.now().date()):
            if 0 < user1['count'] <= 3:
                status = 'loaded'
            elif user1['count'] > 3:
                status = 'overloaded'

        my_json_2.append({
                "measurement": "resource_occupation",
                "tags": {
                    "assignee_username": user1['assignee_username'],
                    "date_tag": str(datetime.now().date()),
                    "status_tag": status
                },
                "time": str(datetime.now().date()),
                "fields": {
                    "status": status,
                    "date": str(datetime.now().date())
                }
            })

    get_resource_occupation_json = client.query("SELECT * FROM resource_occupation")
    get_resource_occupation_json_list = list(get_resource_occupation_json.get_points(measurement="resource_occupation"))
    for measurement1 in my_json_2:
        for measurement2 in get_resource_occupation_json_list:
            if measurement1['tags']['date_tag'] == measurement2['date_tag'] and measurement1['tags']['assignee_username'] == measurement2['assignee_username'] and measurement1['tags']['status_tag'] != measurement2['status_tag']:
                x1 = measurement2['status_tag']
                y1 = "\'" + x1 + "\'"
                x2 = measurement2['assignee_username']
                y2 = "\'" + x2 + "\'"
                x3 = measurement2['date_tag']
                y3 = "\'" + x3 + "\'"
                client.query(f'DELETE FROM resource_occupation WHERE status_tag = {y1} AND assignee_username = {y2} AND date_tag = {y3}')
    client.write_points(my_json_2)


    #### [Incomplete} Time taken from todo to doing and from doing to review

    # get_todo_json = client.query("SELECT * FROM todo")
    # get_todo_json_list = list(get_todo_json.get_points(measurement="todo"))
    #
    # get_doing_json = client.query("SELECT * FROM doing")
    # get_doing_json_list = list(get_doing_json.get_points(measurement="doing"))
    #
    # get_review_json = client.query("SELECT * FROM review")
    # get_review_json_list = list(get_review_json.get_points(measurement="review"))

    # td_json = []
    # dr_json = []
    #
    # for todo_issue in get_todo_json_list:
    #     for doing_issue in get_doing_json_list:
    #         if todo_issue['issue_id_tag'] == doing_issue['issue_id_tag']:
    #             t1 = todo_issue['time']
    #             t2 = doing_issue['time']
    #             t1 = datetime.strptime(t1[:-11], '%Y-%m-%dT%H:%M:%S')
    #             t2 = datetime.strptime(t2[:-11], '%Y-%m-%dT%H:%M:%S')
    #             time_td = t2 - t1
    #             td_json.append({
    #                 "measurement": "timetaken",
    #                 "tags": {
    #                     "assignee_username": todo_issue['assignee_username'],
    #                     "project_name": todo_issue['project_name'],
    #                     "issue_id_tag": todo_issue['issue_id_tag'],
    #                     "time_from_todo_to_doing": time_td
    #                 },
    #                 "time": datetime.now().time(),
    #                 "fields": {
    #                     "time_from_todo_to_doing": time_td
    #                 }
    #             })
    #
    # for doing_issue in get_doing_json_list:
    #     for review_issue in get_review_json_list:
    #         if doing_issue['issue_id_tag'] == review_issue['issue_id_tag']:
    #             t1 = doing_issue['time']
    #             t2 = review_issue['time']
    #             t1 = datetime.strptime(t1[:-11], '%Y-%m-%dT%H:%M:%S')
    #             t2 = datetime.strptime(t2[:-11], '%Y-%m-%dT%H:%M:%S')
    #             time_dr = t2 - t1
    ###########################################################################


    # client.write_points(todo_json)
    # client.write_points(doing_json)
    # client.write_points(review_json)
    now = datetime.now()
    current_time = str(now.time())[:-7]

    splits = current_time.split(":")
    current_time = str(int(splits[0]) - 5) + ":" + splits[1] + ":" + splits[2]
    current_date = now.date()


schedule.every(5).seconds.do(send_request)

while True:
    schedule.run_pending()
    time.sleep(1)

