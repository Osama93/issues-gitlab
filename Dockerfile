FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip3 install --no-cache-dir -r requirements.txt

ENV TOKEN="oCJRA9fvRPntvQgKxG_5"
ENV DATABASE_HOST=localhost
ENV DATABASE_PORT=8086
ENV DATABASE_USER=root
ENV DATABASE_PASS=root

COPY . .

CMD [ "python3", "./gitlabIssues.py" ]
